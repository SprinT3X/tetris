package essentials;

import objects.GameObject;
import java.awt.Graphics;
import java.util.LinkedList;

public class Handler
{
	public static LinkedList<GameObject> object = new LinkedList<>();

	public static void tickAll()
	{
		for (int i = 0; i < object.size(); i++)
		{
			GameObject tempObject = object.get(i);

			tempObject.tick();
		}
	}

	public static void renderAll(Graphics g)
	{
		for (int i = 0; i < object.size(); i++)
		{
			GameObject tempObject = object.get(i);

			tempObject.render(g);
		}
	}

	public static void clear()
	{
		object.clear();
	}

	public static void addObject(GameObject obj)
	{
		object.add(obj);
	}

	public static void removeObject(GameObject obj)
	{
		object.remove(obj);
	}
}