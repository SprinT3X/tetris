package essentials;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import objects.Shape;

public class Display
{
	private static final Font font = new Font("Helvetica", 0, 25); //$NON-NLS-1$
	public static int score = 0;
	public static int lines = 0;
	private static int time = 0;
	private static int timeIncrement = 0;

	public static void tick()
	{
		if (timeIncrement++ == 60)
		{
			time++;
			timeIncrement = 0;
		}
	}

	public static void render(Graphics g)
	{
		g.setColor(Color.darkGray);
		g.fillRect(400, 0, 200, 800);

		g.setColor(Color.white);
		g.setFont(font);
		g.drawString("SCORE: "+String.valueOf(score), 400 + (Game.WIDTH - 400 - g.getFontMetrics().stringWidth("SCORE: "+String.valueOf(score))) / 2, 250); //$NON-NLS-1$ //$NON-NLS-2$
		String min = String.valueOf(time / 60);
		if (time / 60 < 10)
		{
			min = "0" + min; //$NON-NLS-1$
		}
		String sec = String.valueOf(time % 60);
		if (time % 60 < 10)
		{
			sec = "0" + sec; //$NON-NLS-1$
		}
		String timeString = min + ":" + sec; //$NON-NLS-1$
		g.drawString(timeString, 400 + (Game.WIDTH - 400 - g.getFontMetrics().stringWidth(timeString)) / 2, 350);
		g.drawString("LINES: "+String.valueOf(lines), 400 + (Game.WIDTH - 400 - g.getFontMetrics().stringWidth("LINES: "+String.valueOf(lines))) / 2, 300); //$NON-NLS-1$ //$NON-NLS-2$

		switch (Spawner.next)
		{
		case 1:
			drawNext(g, new int[] { 12, 13, 11, 12 }, new int[] { 2, 2, 3, 3 }, Color.green);
			break;
		case 2:
			drawNext(g, new int[] { 11, 12, 12, 13 }, new int[] { 2, 2, 3, 3 }, Color.red);
			break;
		case 3:
			drawNext(g, new int[] { 11, 11, 12, 12 }, new int[] { 2, 3, 2, 3 }, Color.yellow);
			break;
		case 4:
			drawNext(g, new int[] { 11, 11, 11, 12 }, new int[] { 1, 2, 3, 3 }, Color.orange);
			break;
		case 5:
			drawNext(g, new int[] { 12, 12, 12, 12 }, new int[] { 1, 2, 3, 4 }, Color.cyan);
			break;
		case 6:
			drawNext(g, new int[] { 11, 11, 11, 12 }, new int[] { 1, 2, 3, 2 }, Color.pink);
			break;
		case 7:
			drawNext(g, new int[] { 11, 12, 12, 12 }, new int[] { 3, 1, 2, 3 }, Color.blue);
			break;
		default:
			break;

		}
	}

	private static void drawNext(Graphics g, int[] x, int[] y, Color c)
	{
		for (int i = 0; i < 4; i++)
		{
			g.setColor(c);
			g.fillRect(x[i] * Shape.size, y[i] * Shape.size, Shape.size, Shape.size);
			g.setColor(Color.white);
			g.drawRect(x[i] * Shape.size, y[i] * Shape.size, Shape.size, Shape.size);
		}
	}

	public static void reset()
	{
		score = 0;
		lines = 0;
		time = 0;
	}
}
