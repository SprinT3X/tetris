package essentials;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferStrategy;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import environment.menu.Menu;
import environment.menu.Options;
import environment.menu.Scores;
import inputs.KeyInput;
import inputs.MouseClick;
import inputs.MouseMotion;
import objects.Shape;

public class Game extends Canvas implements Runnable
{
	private static final long serialVersionUID = 1L;
	private static final String logFile = "resources/logfile"; //$NON-NLS-1$
	private static final Logger LOGGER = Logger.getLogger(Shape.class.getName());
	@SuppressWarnings("hiding")
	public static final int WIDTH = 600, HEIGHT = 800;
	public static boolean running = true;
	public static boolean gameIsOver;
	public static GameState state;
	private static FileHandler fh;
	public static Window window;

	private Thread thread = new Thread(this);

	public Game()
	{
		initLogger();
		this.addKeyListener(new KeyInput());
		this.addMouseListener(new MouseClick());
		this.addMouseMotionListener(new MouseMotion());
		window = new Window(WIDTH, HEIGHT, "Game", this); //$NON-NLS-1$
		gameIsOver = false;
		state = GameState.Menu;
		Menu.init();
		Options.init();
		Scores.init();
	}

	@Override
	public void run()
	{
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double delta = 0;
		while (running)
		{
			long now = System.nanoTime();
			delta += (now - lastTime) * amountOfTicks / 1000000000;
			lastTime = now;
			while (delta >= 1)
			{
				tick();
				delta--;
			}
			if (running)
			{
				render();
			}
		}
	}

	public static void tick()
	{
		if (state == GameState.Menu)
		{
			Menu.tick();
		} else if (state == GameState.Game)
		{
			if (!gameIsOver)
			{
				Spawner.tick();
				Handler.tickAll();
				Display.tick();
			}
		} else if (state == GameState.Options)
		{
			Options.tick();
		} else if (state == GameState.Highscores)
		{
			Scores.tick();
		}
	}

	public void render()
	{
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null)
		{
			this.createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();
		Graphics2D g2 = (Graphics2D) g;
		if (Options.antialiasing)
		{
			g2.setRenderingHints(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
		}

		g2.setColor(Color.black);
		g2.fillRect(0, 0, WIDTH + Window.inSetWidth, HEIGHT + Window.inSetHeight);

		if (state == GameState.Menu)
		{
			Menu.render(g2);
		} else if (state == GameState.Game)
		{
			Handler.renderAll(g2);
			Display.render(g2);
			if (gameIsOver)
			{
				gameOver(g2);
			}
			g.setColor(Color.white);
			g.drawLine(400, 0, 400, HEIGHT);
		} else if (state == GameState.Options)
		{
			Options.render(g2);
		} else if (state == GameState.Highscores)
		{
			Scores.render(g2);
		}

		g.dispose();
		bs.show();
	}

	private static void gameOver(Graphics g)
	{
		String game = "GAME", over = "OVER", returnToMenu = "Press Escape!"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		Font gameOverFont = new Font("Helvetica", 1, 120); //$NON-NLS-1$
		Font returnFont = new Font("Helvetica", 2, 45); //$NON-NLS-1$
		g.setFont(gameOverFont);
		g.setColor(Color.white);
		g.drawString(game, (Game.WIDTH - 200 - g.getFontMetrics().stringWidth(game)) / 2, 300);
		g.drawString(over, (Game.WIDTH - 200 - g.getFontMetrics().stringWidth(over)) / 2, 500);
		g.setFont(returnFont);
		g.setColor(Color.yellow);
		g.drawString(returnToMenu, (Game.WIDTH - 200 - g.getFontMetrics().stringWidth(returnToMenu)) / 2, 650);
	}

	private static void initLogger()
	{
		System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT [%4$s] [%2$s] : %5$s%6$s%n"); //$NON-NLS-1$ //$NON-NLS-2$
		try
		{
			fh = new FileHandler(logFile, true);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			LOGGER.addHandler(fh);
		} catch (SecurityException | IOException e)
		{
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		new Game().thread.start();
	}

}
