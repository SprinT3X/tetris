package essentials;

import java.util.Random;
import objects.Shape;
import objects.Square;

public class Spawner
{
	private static Random r = new Random();
	private static int speedTimer = 0;
	public static int next = r.nextInt(7) + 1;

	public static void tick()
	{
		int combo = 0;
		for (int i = 0; i < 20; i++)
		{
			boolean lineIsFull = true;
			for (int j = 0; j < 10; j++)
			{
				if (Square.getSquare(j * Shape.size + Shape.size / 2, i * Shape.size + Shape.size / 2) == null)
				{
					lineIsFull = false;
				}
			}
			if (lineIsFull && Shape.activeShape == null)
			{
				for (int j = 0; j < 10; j++)
				{
					Shape.getShape(j * Shape.size + Shape.size / 2, i * Shape.size + Shape.size / 2).getSquares().remove(Square.getSquare(j * Shape.size + Shape.size / 2, i * Shape.size + Shape.size / 2));
				}
				for (int k = 0; k < Handler.object.size(); k++)
				{
					Shape shape = (Shape) Handler.object.get(k);
					for (Square square : shape.getSquares())
					{
						if (square.getY() < i * Shape.size + Shape.size / 2)
						{
							square.setY(square.getY() + 40);
						}
					}
				}
				combo++;
			}
		}
		Display.lines+=combo;
		if (combo > 0)
		{
			Display.score += combo * combo * 10 + (50 - Shape.staticMovingDelay) * 5;
		}
		if (Shape.activeShape == null)
		{
			switch (next)
			{
				case 1:
					Handler.addObject(new Shape(Shape.size * 5, -Shape.size, ID.Shape_S));
					break;
				case 2:
					Handler.addObject(new Shape(Shape.size * 5, -Shape.size, ID.Shape_Z));
					break;
				case 3:
					Handler.addObject(new Shape(Shape.size * 5, 0, ID.Shape_O));
					break;
				case 4:
					Handler.addObject(new Shape(Shape.size * 5, 0, ID.Shape_L));
					break;
				case 5:
					Handler.addObject(new Shape(Shape.size * 4, -Shape.size, ID.Shape_I));
					break;
				case 6:
					Handler.addObject(new Shape(Shape.size * 4, 0, ID.Shape_T));
					break;
				case 7:
					Handler.addObject(new Shape(Shape.size * 5, 0, ID.Shape_J));
					break;
				default:
					break;
			}
			next = r.nextInt(7) + 1;
		}

		if (speedTimer++ == 300 && Shape.staticMovingDelay > 5)
		{
			Shape.staticMovingDelay--;
			speedTimer = 0;
		}

	}
}
