package essentials;

import javax.swing.JFrame;

import inputs.KeyInput;

public class Window
{
	public static final int inSetWidth = 6;
	public static final int inSetHeight = 30;
	
	public static JFrame frame;
	
	public Window(int width, int height, String title, Game game)
	{
		frame = new JFrame(title);
		frame.setSize(width+inSetWidth, height+inSetHeight);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.add(game);
		frame.setVisible(true);
		frame.addKeyListener(new KeyInput());
		frame.requestFocus();
	}

}
