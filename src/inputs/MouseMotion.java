package inputs;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class MouseMotion implements MouseMotionListener
{
	public static int mouseX;
	public static int mouseY;

	@Override
	public void mouseDragged(MouseEvent e)
	{
		mouseMoved(e);
		
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		mouseX=e.getX();
		mouseY=e.getY();
		
	}

}
