package inputs;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import environment.UserInput;
import environment.menu.Scores;
import essentials.Display;
import essentials.Game;
import essentials.GameState;
import essentials.Window;
import objects.Shape;

public class KeyInput extends KeyAdapter
{
	@SuppressWarnings("unused")
	@Override
	public void keyPressed(KeyEvent e)
	{
		int key = e.getKeyCode();
		if (Game.state == GameState.Game)
		{
			if (!Game.gameIsOver)
			{
				if (key == KeyEvent.VK_ESCAPE)
				{
					Game.gameIsOver = true;
					Shape.activeShape = null;
					if (Display.score > Scores.minScore)
					{
						new UserInput();
						Window.frame.setEnabled(false);
					}
				} else if (key == KeyEvent.VK_UP)
				{
					if (Shape.activeShape != null)
					{
						Shape.activeShape.rotate();
					}

				} else if (key == KeyEvent.VK_DOWN)
				{
					if (Shape.activeShape != null)
					{
						Shape.activeShape.setDown(true);
					}
				} else if (key == KeyEvent.VK_LEFT)
				{
					if (Shape.activeShape != null)
					{
						Shape.activeShape.left();
					}
				} else if (key == KeyEvent.VK_RIGHT)
				{
					if (Shape.activeShape != null)
					{
						Shape.activeShape.right();
					}
				}
			} else
			{
				if (key == KeyEvent.VK_ESCAPE)
				{
					Game.state = GameState.Menu;
				}
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_DOWN)
		{
			if (Shape.activeShape != null)
			{
				Shape.activeShape.setDown(false);
			}
		}
	}

}
