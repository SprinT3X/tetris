package inputs;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import environment.menu.Menu;
import environment.menu.Options;
import environment.menu.Scores;
import essentials.Display;
import essentials.Game;
import essentials.GameState;
import essentials.Handler;

public class MouseClick implements MouseListener
{

	@Override
	public void mousePressed(MouseEvent e)
	{
		if(Game.state==GameState.Menu)
		{
			if(Menu.play.mouseIsIn())
			{
				Handler.clear();
				Display.reset();
				Game.gameIsOver=false;
				Game.state= GameState.Game;
			} else if(Menu.options.mouseIsIn())
			{
				Game.state=GameState.Options;
			}
			else if(Menu.highscores.mouseIsIn())
			{
				Scores.init();
				Game.state=GameState.Highscores;
			}
			else if(Menu.exit.mouseIsIn())
			{
				System.exit(0);
			}
		}
		else if(Game.state==GameState.Options)
		{
			if(Options.back.mouseIsIn())
			{
				Game.state=GameState.Menu;
			}
			else if(Options.showTargetCB.mouseIsIn())
			{
				Options.showTarget= !Options.showTarget;
				Options.showTargetCB.checked = Options.showTarget;
				Options.updateFile();
			}
			else if(Options.antialiasingCB.mouseIsIn())
			{
				Options.antialiasing= !Options.antialiasing;
				Options.antialiasingCB.checked = Options.antialiasing;
				Options.updateFile();
			}
		}
		else if(Game.state==GameState.Highscores)
		{
			if(Scores.back.mouseIsIn())
			{
				Game.state=GameState.Menu;
			}
		}
		
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

}
