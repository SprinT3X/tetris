package environment;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import environment.menu.Menu;
import environment.menu.Options;
import essentials.Game;
import essentials.GameState;
import inputs.MouseMotion;

public class Button
{
	private int y;
	private int currentY;
	private String text;
	private Font font;
	private Rectangle hitBox;
	private Color color;

	public Button(int y, String text, Font font)
	{
		this.y = y;
		this.text = text;
		this.font = font;
		this.hitBox = new Rectangle(0, 0, 0, 0);
		this.color = Color.white;
		this.currentY = this.y;
	}

	public void tick()
	{
		float size= 0;
		if (Game.state == GameState.Menu)
		{
			size = Menu.fontSize;
		}
		else if(Game.state==GameState.Options)
		{
			size = Options.backFont.getSize();
		}
		else if(Game.state==GameState.Highscores)
		{
			size = Options.backFont.getSize();
		}
		if (this.mouseIsIn())
		{
			this.color = Color.red;
			this.font = this.font.deriveFont(size * 1.1f);
			this.currentY = this.y - (int) size/25;
		} else
		{
			this.currentY = this.y;
			this.font = this.font.deriveFont(size * 1f);
			this.color = Color.white;
		}
	}

	public void render(Graphics g)
	{
		g.setFont(this.font);
		int width = g.getFontMetrics().stringWidth(this.text);
		int height = g.getFontMetrics().getHeight();
		int x = (Game.WIDTH - width) / 2;
		g.setColor(this.color);
		this.hitBox = new Rectangle(x, this.currentY, width, height);
		//g.drawRect(x, this.currentY, width, height);
		g.drawString(this.text, x, this.currentY + height * 4 / 5);
	}

	public Rectangle getHitBox()
	{
		return this.hitBox;
	}

	public boolean mouseIsIn()
	{
		if (this.hitBox.contains(MouseMotion.mouseX, MouseMotion.mouseY))
		{
			return true;
		}
		return false;
	}
}
