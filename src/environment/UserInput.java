package environment;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import environment.menu.Scores;
import essentials.Window;

public class UserInput
{
	private static final String MESSAGE = "nickname"; //$NON-NLS-1$
	private static final Font LABEL_FONT = new Font("Helvetica", 0, 35); //$NON-NLS-1$
	private static final int WIDTH = 300;
	private static final int HEIGHT = 200;
	public static String nickname;
	public static JFrame frame;
	public static JPanel panel;
	public static JTextField tf;

	public UserInput()
	{
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setUndecorated(true);
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.requestFocus();

		panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.lightGray);
		frame.add(panel);

		JLabel label = new JLabel();
		label.setFont(LABEL_FONT);
		label.setText(MESSAGE);
		label.setSize(label.getFontMetrics(LABEL_FONT).stringWidth(MESSAGE), 50);
		label.setLocation((WIDTH - label.getSize().width) / 2, 10);
		label.setForeground(Color.black);
		panel.add(label);

		tf = new JTextField();
		tf.setLocation(50, 70);
		tf.setSize(200, 30);
		tf.addKeyListener(new UserKeyInput());
		panel.add(tf);

		JButton button = new JButton();
		button.setSize(70, 50);
		button.setLocation(115, 120);
		button.setFocusable(false);
		button.setText("OK"); //$NON-NLS-1$
		button.setBackground(Color.white);
		button.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				nickname = tf.getText();
				Scores.checkHighScore();
				frame.setVisible(false);
				Window.frame.setEnabled(true);
				Window.frame.requestFocus();
			}
		});
		panel.add(button);
		frame.pack();
		frame.setSize(WIDTH, HEIGHT);
		frame.setVisible(true);
		tf.grabFocus();
	}
}

class UserKeyInput implements KeyListener
{
	@Override
	public void keyPressed(KeyEvent e)
	{
		int key = e.getKeyCode();
		if(key==KeyEvent.VK_ENTER)
		{
			UserInput.nickname = UserInput.tf.getText();
			Scores.checkHighScore();
			UserInput.frame.setVisible(false);
			Window.frame.setEnabled(true);
			Window.frame.requestFocus();
		}
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		// TODO Auto-generated method stub
		
	}
	
}
