package environment.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import environment.Button;
import environment.UserInput;
import essentials.Display;
import essentials.Game;

public class Scores
{
	private static final String filePath = "resources/highscores"; //$NON-NLS-1$
	private static final Font rowFont = new Font("Helvetica", 2, 30); //$NON-NLS-1$
	private static final Font coloumnFont = new Font("Helvetica", 1, 40); //$NON-NLS-1$
	private static final Font hsFont = new Font("Helvetica", 1, 60); //$NON-NLS-1$
	private static List<Row> rows = new ArrayList<>();
	public static Button back;
	public static int minScore;

	@SuppressWarnings("resource")
	public static void init()
	{
		rows = new ArrayList<>();
		back = new Button(Game.HEIGHT - 100, "BACK", Options.backFont); //$NON-NLS-1$
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(new File(filePath)));
			String line;
			String[] data;
			while ((line = br.readLine()) != null)
			{
				data = line.split(";"); //$NON-NLS-1$
				rows.add(new Row(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2])));

			}
			br.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		orderRows();
		minScore = rows.get(rows.size()-1).getScore();
	}

	public static void tick()
	{
		back.tick();
	}

	public static void render(Graphics g)
	{
		back.render(g);
		g.setColor(Color.green);
		g.setFont(hsFont);
		g.drawString("HIGHSCORES", (Game.WIDTH - g.getFontMetrics().stringWidth("HIGHSCORES")) / 2, g.getFontMetrics().getHeight() - 15); //$NON-NLS-1$ //$NON-NLS-2$
		g.setColor(Color.white);
		g.drawLine(0, g.getFontMetrics().getHeight(), Game.WIDTH, g.getFontMetrics().getHeight());
		g.drawLine(0, g.getFontMetrics().getHeight() + 80, Game.WIDTH, g.getFontMetrics().getHeight() + 80);
		g.drawLine(Game.WIDTH / 3, g.getFontMetrics().getHeight(), Game.WIDTH / 3, 675);
		g.drawLine(Game.WIDTH * 2 / 3, g.getFontMetrics().getHeight(), Game.WIDTH * 2 / 3, 675);
		g.drawLine(0, 675, Game.WIDTH, 675);
		g.setFont(coloumnFont);
		g.drawString("Name", (Game.WIDTH / 3 - g.getFontMetrics().stringWidth("Name")) / 2, 130); //$NON-NLS-1$ //$NON-NLS-2$
		g.drawString("Score", (Game.WIDTH / 3 - g.getFontMetrics().stringWidth("Score")) / 2 + Game.WIDTH / 3, 130); //$NON-NLS-1$ //$NON-NLS-2$
		g.drawString("Lines", (Game.WIDTH / 3 - g.getFontMetrics().stringWidth("Lines")) / 2 + Game.WIDTH * 2 / 3, 130); //$NON-NLS-1$ //$NON-NLS-2$
		g.setFont(rowFont);
		for (int i = 0; i < rows.size(); i++)
		{
			Row row = rows.get(i);
			while (g.getFontMetrics().stringWidth(row.getName()) > Game.WIDTH / 3)
			{
				row.setName(row.getName().substring(0, row.getName().length() - 1));
			}
			g.drawString(row.getName(), (Game.WIDTH / 3 - g.getFontMetrics().stringWidth(row.getName())) / 2, 50 * i + 200);
			g.drawString(String.valueOf(row.getScore()), (Game.WIDTH / 3 - g.getFontMetrics().stringWidth(String.valueOf(row.getScore()))) / 2 + Game.WIDTH / 3, 50 * i + 200);
			g.drawString(String.valueOf(row.getLines()), (Game.WIDTH / 3 - g.getFontMetrics().stringWidth(String.valueOf(row.getLines()))) / 2 + Game.WIDTH * 2 / 3, 50 * i + 200);
		}
	}

	private static void orderRows()
	{
		Integer[] scores = new Integer[rows.size()];
		for (int i = 0; i < rows.size(); i++)
		{
			scores[i] = Integer.valueOf(rows.get(i).getScore());
		}
		Arrays.sort(scores, Collections.reverseOrder());
		List<Row> newList = new ArrayList<>();
		for (int i = 0; i < scores.length; i++)
		{
			for (int j = 0; j < rows.size(); j++)
			{
				Row r = rows.get(j);
				if (r.getScore() == scores[i].intValue())
				{
					newList.add(new Row(r.getName(), r.getScore(), r.getLines()));
					rows.remove(j);
					j--;
				}
			}
		}
		rows = newList;
	}

	public static void checkHighScore()
	{
		orderRows();
		String nick = UserInput.nickname;
		if (Display.score > rows.get(rows.size() - 1).getScore())
		{
			if (nick != null)
			{
				if (nick.equals("")) //$NON-NLS-1$
				{
					nick = "Player"; //$NON-NLS-1$
				}
				for (int i = 0; i < rows.size(); i++)
				{
					if (Display.score > rows.get(i).getScore())
					{
						rows.add(i, new Row(nick, Display.score, Display.lines));
						break;
					}
				}
				rows.remove(rows.size() - 1);
				try
				{
					@SuppressWarnings("resource")
					BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filePath)));
					for (int i = 0; i < rows.size(); i++)
					{
						Row row = rows.get(i);
						bw.write(row.getName() + ";" + row.getScore() + ";" + row.getLines() + System.lineSeparator()); //$NON-NLS-1$ //$NON-NLS-2$
					}
					bw.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}
