package environment.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import environment.Button;
import environment.CheckBox;
import essentials.Game;

public class Options
{
	public static final Font backFont = new Font("Helvetica",0,50); //$NON-NLS-1$
	public static final Font textFont = new Font("Helvetica",2,35); //$NON-NLS-1$
	public static final String FILE_PATH = "resources/options"; //$NON-NLS-1$
	public static boolean showTarget;
	public static boolean antialiasing;
	public static Button back;
	public static CheckBox showTargetCB;
	public static CheckBox antialiasingCB;
	
	public static void init()
	{
		try
		{
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(new File(FILE_PATH)));
			showTarget = Boolean.parseBoolean(br.readLine());
			antialiasing = Boolean.parseBoolean(br.readLine());	
			br.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		back = new Button(Game.HEIGHT-100, "BACK", backFont); //$NON-NLS-1$
		showTargetCB = new CheckBox(Game.WIDTH-100, 100, 49, 49, showTarget);
		antialiasingCB = new CheckBox(Game.WIDTH-100, 200, 49, 49, antialiasing);
	}
	
	public static void tick()
	{
		back.tick();
		showTargetCB.tick();
		antialiasingCB.tick();
	}
	
	public static void render(Graphics g)
	{
		back.render(g);
		showTargetCB.render(g);
		antialiasingCB.render(g);
		g.setColor(Color.white);
		g.setFont(textFont);
		g.drawString("SHOW RESULT SHAPE", 50, 150); //$NON-NLS-1$
		g.drawString("ANTIALIASING", 50, 250); //$NON-NLS-1$
	}
	
	public static void updateFile()
	{
		try
		{
			@SuppressWarnings("resource")
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(FILE_PATH)));
			bw.write(Options.showTarget+System.lineSeparator());
			bw.write(Options.antialiasing+System.lineSeparator());
			bw.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
}
