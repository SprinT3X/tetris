package environment.menu;

public class Row
{
	private String name;
	private int score;
	private int lines;

	public Row(String name, int score, int lines)
	{
		super();
		this.name = name;
		this.score = score;
		this.lines = lines;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return this.name;
	}

	public int getScore()
	{
		return this.score;
	}

	public int getLines()
	{
		return this.lines;
	}

	@Override
	public String toString()
	{
		return this.name + " " + this.score + " " + this.lines; //$NON-NLS-1$ //$NON-NLS-2$
	}

}
