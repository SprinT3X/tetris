package environment.menu;

import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import environment.Button;

public class Menu
{
	public static final int fontSize= 100;
	
	public static List<Button> buttons = new ArrayList<>();
	public static Button play;
	public static Button options;
	public static Button highscores;
	public static Button exit;
	
	
	public static void init()
	{
		Font font = new Font("Helvetica",0,fontSize); //$NON-NLS-1$
		play = new Button(150,"PLAY",font); //$NON-NLS-1$
		options=new Button(300,"OPTIONS",font); //$NON-NLS-1$
		highscores = new Button(450,"SCORES",font); //$NON-NLS-1$
		exit = new Button(600,"EXIT",font); //$NON-NLS-1$
		buttons.add(play);
		buttons.add(options);
		buttons.add(highscores);
		buttons.add(exit);
	}

	public static void tick()
	{
		for(Button button: buttons)
		{
			button.tick();
		}
	}

	public static void render(Graphics g)
	{
		for(Button button: buttons)
		{
			button.render(g);
		}
	}
}
