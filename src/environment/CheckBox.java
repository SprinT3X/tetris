package environment;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import inputs.MouseMotion;

public class CheckBox
{
	private Rectangle hitBox;
	public boolean checked;

	public CheckBox(int x, int y, int width, int height, boolean checked)
	{
		this.hitBox = new Rectangle(x, y, width, height);
		this.checked=checked;

	}
	
	public void tick()
	{
		//TODO
	}
	
	public void render(Graphics g)
	{
		g.setColor(Color.white);
		g.drawRect(this.hitBox.x, this.hitBox.y, this.hitBox.width, this.hitBox.height);
		
		if(this.checked)
		{
			g.setColor(Color.green);
			g.fillRect(this.hitBox.x+6, this.hitBox.y+6, this.hitBox.width-12, this.hitBox.height-12);
		}
	}
	
	public boolean mouseIsIn()
	{
		if (this.hitBox.contains(MouseMotion.mouseX, MouseMotion.mouseY))
		{
			return true;
		}
		return false;
	}

	public Rectangle getHitBox()
	{
		return this.hitBox;
	}

}
