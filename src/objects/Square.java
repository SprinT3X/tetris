package objects;

import essentials.Handler;
import essentials.ID;
import java.awt.Color;
import java.awt.Graphics;
import java.util.logging.Logger;

public class Square extends GameObject
{
	private static final Logger LOGGER = Logger.getLogger(Shape.class.getName());
	public static final Color targetFillColor = new Color(255, 255, 255, 40);

	public Square(int x, int y, ID id)
	{
		super(x, y, id);
	}

	@Override
	public void tick()
	{
		// TODO
	}

	@Override
	public void render(Graphics g)
	{
		g.fillRect(this.x, this.y, Shape.size, Shape.size);

		g.setColor(Color.white);
		g.drawRect(this.x, this.y, Shape.size, Shape.size);
	}

	public void renderTarget(Graphics g, int deep)
	{
		g.setColor(targetFillColor);
		g.fillRect(this.x, this.y + deep * Shape.size, Shape.size, Shape.size);
		g.setColor(Color.red);
		g.drawRect(this.x, this.y + deep * Shape.size, Shape.size, Shape.size);
	}

	public static Square getSquare(int x, int y)
	{
		for (int i = 0; i < Handler.object.size(); i++)
		{
			Shape shape = (Shape) Handler.object.get(i);
			if(shape==null)
			{
				LOGGER.warning("The shape was nul!"); //$NON-NLS-1$
				continue;
			}
			for (int j = 0; j < shape.getSquares().size(); j++)
			{
				Square s = shape.getSquares().get(j);
				if (s != null)
				{
					if (x > s.x && x < s.x + Shape.size && y > s.y && y < s.y + Shape.size)
					{
						return s;
					}
				} else
				{
					LOGGER.warning("The square was null!"); //$NON-NLS-1$
				}
			}
		}
		return null;
	}

	@Override
	public String toString()
	{
		return this.x + " " + this.y; //$NON-NLS-1$
	}
}
