package objects;

import essentials.Display;
import essentials.Game;
import essentials.Handler;
import essentials.ID;
import essentials.Window;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import environment.UserInput;
import environment.menu.Options;
import environment.menu.Scores;

public class Shape extends GameObject
{
	private static final Logger LOGGER = Logger.getLogger(Shape.class.getName());
	public static final int size = 40;
	public static int staticMovingDelay = 50;
	public static Shape activeShape = null;

	private int movingDelay;
	private List<Square> squares;
	private boolean down;
	private int rotated;
	private int rotateDelay = 5;
	private int minDeep;

	public Shape(int x, int y, ID id)
	{
		super(x, y, id);
		activeShape = this;
		this.movingDelay = staticMovingDelay;
		buildShape();
		this.rotated = 0;
		this.minDeep = 0;
	}

	@SuppressWarnings("unused")
	@Override
	public void tick()
	{
		this.movingDelay--;
		if (this.rotateDelay > 0)
		{
			this.rotateDelay--;
		}
		for (int i = 0; i < this.squares.size(); i++)
		{
			Square s = this.squares.get(i);
			Square next = Square.getSquare(s.x + size / 2, s.y + size * 3 / 2);
			boolean contains = false;
			for (int j = 0; j < this.squares.size(); j++)
			{
				Square s2 = this.squares.get(j);
				if (s2.equals(next))
				{
					contains = true;
				}
			}
			if ((!contains && next != null) || s.y + size == Game.HEIGHT)
			{
				this.down = false;
				if (this.movingDelay == 0)
				{
					for (int j = 0; j < activeShape.getSquares().size(); j++)
					{
						Square square = activeShape.getSquares().get(j);
						if (square.y < 0)
						{
							Game.gameIsOver = true;
							activeShape=null;
							if (Display.score > Scores.minScore)
							{
								new UserInput();
								Window.frame.setEnabled(false);
							}
							return;
						}
					}
					activeShape = null;
				}
				break;
			}
		}
		if (activeShape == this)
		{
			if (this.movingDelay == 0 || this.down)
			{
				this.setMovingDelay(staticMovingDelay);
				if (this.down)
				{
					this.setMovingDelay(5);
				}
				for (Square s : this.squares)
				{
					s.y += 40;
				}
				this.y += 40;
			}
		}
		this.minDeep = 30;
		for (int i = 0; i < this.squares.size(); i++)
		{
			int deep = 0;
			Square s = this.squares.get(i);
			for (int j = 1; j < 30; j++)
			{
				Square target = Square.getSquare(s.x + Shape.size / 2, s.y + Shape.size / 2 + j * Shape.size);
				boolean contains = false;
				for (Square s2 : this.squares)
				{
					if (s2.equals(target))
					{
						contains = true;
					}
				}
				if (target == null || contains)
				{
					if (s.y + Shape.size / 2 + j * Shape.size > Game.HEIGHT)
					{
						break;
					}
					deep++;
				} else
				{
					break;
				}
			}
			if (deep < this.minDeep)
			{
				this.minDeep = deep;
			}
		}
	}

	@Override
	public void render(Graphics g)
	{
		for (int i = 0; i < this.squares.size(); i++)
		{
			Square s = this.squares.get(i);
			if (activeShape == this && Options.showTarget && s != null)
			{
				s.renderTarget(g, this.minDeep);
			}
			switch (this.id)
			{
			case Shape_S:
				g.setColor(Color.green);
				break;
			case Shape_Z:
				g.setColor(Color.red);
				break;
			case Shape_O:
				g.setColor(Color.yellow);
				break;
			case Shape_L:
				g.setColor(Color.orange);
				break;
			case Shape_I:
				g.setColor(Color.cyan);
				break;
			case Shape_T:
				g.setColor(Color.pink);
				break;
			case Shape_J:
				g.setColor(Color.blue);
				break;
			default:
				break;
			}
			if (s != null)
			{
				s.render(g);
			} else
			{
				LOGGER.warning("Square could not be rendered!"); //$NON-NLS-1$
			}
		}
	}

	private void buildShape()
	{
		this.squares = new ArrayList<>();
		switch (this.id)
		{
		case Shape_S:
			this.squares.add(new Square(this.x, this.y, ID.Square));
			this.squares.add(new Square(this.x + size, this.y, ID.Square));
			this.squares.add(new Square(this.x - size, this.y + size, ID.Square));
			this.squares.add(new Square(this.x, this.y + size, ID.Square));
			break;
		case Shape_Z:
			this.squares.add(new Square(this.x - 2 * size, this.y, ID.Square));
			this.squares.add(new Square(this.x - size, this.y, ID.Square));
			this.squares.add(new Square(this.x - size, this.y + size, ID.Square));
			this.squares.add(new Square(this.x, this.y + size, ID.Square));
			break;
		case Shape_O:
			this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
			this.squares.add(new Square(this.x - size, this.y, ID.Square));
			this.squares.add(new Square(this.x, this.y - size, ID.Square));
			this.squares.add(new Square(this.x, this.y, ID.Square));
			break;
		case Shape_L:
			this.squares.add(new Square(this.x - size, this.y - 2 * size, ID.Square));
			this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
			this.squares.add(new Square(this.x - size, this.y, ID.Square));
			this.squares.add(new Square(this.x, this.y, ID.Square));
			break;
		case Shape_I:
			this.squares.add(new Square(this.x, this.y - 2 * size, ID.Square));
			this.squares.add(new Square(this.x, this.y - size, ID.Square));
			this.squares.add(new Square(this.x, this.y, ID.Square));
			this.squares.add(new Square(this.x, this.y + size, ID.Square));
			break;
		case Shape_T:
			this.squares.add(new Square(this.x, this.y - 2 * size, ID.Square));
			this.squares.add(new Square(this.x, this.y - size, ID.Square));
			this.squares.add(new Square(this.x, this.y, ID.Square));
			this.squares.add(new Square(this.x + size, this.y - size, ID.Square));
			break;
		case Shape_J:
			this.squares.add(new Square(this.x - size, this.y, ID.Square));
			this.squares.add(new Square(this.x, this.y - 2 * size, ID.Square));
			this.squares.add(new Square(this.x, this.y - size, ID.Square));
			this.squares.add(new Square(this.x, this.y, ID.Square));
			break;

		default:
			break;
		}
	}

	public void left()
	{
		boolean canMove = true;
		for (int i = 0; i < this.squares.size(); i++)
		{
			Square s = this.squares.get(i);
			Square next = Square.getSquare(s.x - size / 2, s.y + size / 2);
			boolean contains = false;
			for (int j = 0; j < this.squares.size(); j++)
			{
				Square s2 = this.squares.get(j);
				if (s2.equals(next))
				{
					contains = true;
				}
			}
			if ((!contains && next != null) || s.x == 0)
			{
				canMove = false;
			}
		}
		if (canMove)
		{
			for (int j = 0; j < this.squares.size(); j++)
			{
				Square s = this.squares.get(j);
				s.x -= size;
			}
			this.x -= size;
		}
	}

	public void right()
	{
		boolean canMove = true;
		for (int i = 0; i < this.squares.size(); i++)
		{
			Square s = this.squares.get(i);
			Square next = Square.getSquare(s.x + size * 3 / 2, s.y + size / 2);
			boolean contains = false;
			for (int j = 0; j < this.squares.size(); j++)
			{
				Square s2 = this.squares.get(j);
				if (s2.equals(next))
				{
					contains = true;
				}

			}
			if ((!contains && next != null) || s.x == 9 * size)
			{
				canMove = false;
			}
		}
		if (canMove)
		{
			for (int j = 0; j < this.squares.size(); j++)
			{
				Square s = this.squares.get(j);
				s.x += size;
			}
			this.x += size;
		}
	}

	public void rotate()
	{
		if (this.rotateDelay > 0)
		{
			return;
		}
		boolean couldRotate = false;
		switch (this.id)
		{
		case Shape_S:
			if (this.rotated == 0 || this.rotated == 2)
			{
				if (Square.getSquare(this.x - size / 2, this.y - size / 2) == null && Square.getSquare(this.x - size / 2, this.y + size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x - size, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y + size, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 1 || this.rotated == 3)
			{
				if (this.x == 9 * size)
				{
					return;
				}
				if (Square.getSquare(this.x - size / 2, this.y + size * 3 / 2) == null && Square.getSquare(this.x + size * 3 / 2, this.y + size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x + size, this.y, ID.Square));
					this.squares.add(new Square(this.x - size, this.y + size, ID.Square));
					this.squares.add(new Square(this.x, this.y + size, ID.Square));
					couldRotate = true;
				}
			}
			break;
		case Shape_Z:
			if (this.rotated == 0 || this.rotated == 2)
			{
				if (Square.getSquare(this.x + size / 2, this.y + size / 2) == null && Square.getSquare(this.x + size / 2, this.y - size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x - size, this.y, ID.Square));
					this.squares.add(new Square(this.x - size, this.y + size, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 1 || this.rotated == 3)
			{
				if (this.x == size)
				{
					return;
				}
				if (Square.getSquare(this.x - size * 3 / 2, this.y + size / 2) == null && Square.getSquare(this.x + size / 2, this.y + size * 3 / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - 2 * size, this.y, ID.Square));
					this.squares.add(new Square(this.x - size, this.y, ID.Square));
					this.squares.add(new Square(this.x - size, this.y + size, ID.Square));
					this.squares.add(new Square(this.x, this.y + size, ID.Square));
					couldRotate = true;
				}
			}
			break;
		case Shape_O:
			break;
		case Shape_L:
			if (this.rotated == 0)
			{
				if (this.x == 9 * Shape.size)
				{
					return;
				}
				if (Square.getSquare(this.x + size / 2, this.y - size / 2) == null && Square.getSquare(this.x + size * 3 / 2, this.y - size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x - size, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x + size, this.y - size, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 1)
			{
				if (this.y == size * 19)
				{
					return;
				}
				if (Square.getSquare(this.x + size / 2, this.y + size / 2) == null && Square.getSquare(this.x + size / 2, this.y + size * 3 / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y + size, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 2)
			{
				if (this.x == Shape.size)
				{
					return;
				}
				if (Square.getSquare(this.x - size * 3 / 2, this.y + size / 2) == null && Square.getSquare(this.x - size / 2, this.y + size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - 2 * size, this.y, ID.Square));
					this.squares.add(new Square(this.x - size, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 3)
			{
				if (Square.getSquare(this.x - size / 2, this.y - size * 3 / 2) == null && Square.getSquare(this.x - size / 2, this.y - size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - size, this.y - 2 * size, ID.Square));
					this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x - size, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y, ID.Square));
					couldRotate = true;
				}
			}
			break;
		case Shape_I:
			if (this.rotated == 0 || this.rotated == 2)
			{
				if (this.x == 0 || this.x == size || this.x == 9 * size)
				{
					return;
				}
				if (Square.getSquare(this.x - size * 3 / 2, this.y + size / 2) == null && Square.getSquare(this.x - size / 2, this.y + size / 2) == null && Square.getSquare(this.x + size * 3 / 2, this.y + size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - 2 * size, this.y, ID.Square));
					this.squares.add(new Square(this.x - size, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x + size, this.y, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 1 || this.rotated == 3)
			{
				if (this.y == 19 * size)
				{
					return;
				}
				if (Square.getSquare(this.x + size / 2, this.y - size * 3 / 2) == null && Square.getSquare(this.x + size / 2, this.y - size / 2) == null && Square.getSquare(this.x + size / 2, this.y + size * 3 / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x, this.y - 2 * size, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y + size, ID.Square));
					couldRotate = true;
				}
			}
			break;
		case Shape_T:
			if (this.rotated == 0)
			{
				if (this.x == 0)
				{
					return;
				}
				if (Square.getSquare(this.x - size / 2, this.y - size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x + size, this.y - size, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 1)
			{
				if (Square.getSquare(this.x + size / 2, this.y - size * 3 / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y - 2 * size, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 2)
			{
				if (this.x == 9 * size)
				{
					return;
				}
				if (Square.getSquare(this.x + size * 3 / 2, this.y - size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x + size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y - 2 * size, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 3)
			{
				if (this.y == 20 * size)
				{
					return;
				}
				if (Square.getSquare(this.x + size / 2, this.y + size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x + size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y - 2 * size, ID.Square));
					couldRotate = true;
				}
			}
			break;
		case Shape_J:
			if (this.rotated == 0)
			{
				if (this.x == 9 * size)
				{
					return;
				}
				if (Square.getSquare(this.x - size / 2, this.y - size / 2) == null && Square.getSquare(this.x + size * 3 / 2, this.y + size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x - size, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y, ID.Square));
					this.squares.add(new Square(this.x + size, this.y, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 1)
			{
				if (this.y == 19 * size)
				{
					return;
				}
				if (Square.getSquare(this.x + size / 2, this.y - size / 2) == null && Square.getSquare(this.x - size / 2, this.y + size * 3 / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x - size, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x - size, this.y + size, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 2)
			{
				if (this.x == size)
				{
					return;
				}
				if (Square.getSquare(this.x + size / 2, this.y + size / 2) == null && Square.getSquare(this.x - size * 3 / 2, this.y - size / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - 2 * size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x - size, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y, ID.Square));
					couldRotate = true;
				}
			} else if (this.rotated == 3)
			{
				if (Square.getSquare(this.x - size / 2, this.y + size / 2) == null && Square.getSquare(this.x + size / 2, this.y - size * 3 / 2) == null)
				{
					this.squares = new ArrayList<>();
					this.squares.add(new Square(this.x - size, this.y, ID.Square));
					this.squares.add(new Square(this.x, this.y - 2 * size, ID.Square));
					this.squares.add(new Square(this.x, this.y - size, ID.Square));
					this.squares.add(new Square(this.x, this.y, ID.Square));
					couldRotate = true;
				}
			}
			break;
		default:
			break;
		}
		if (couldRotate)
		{
			this.rotated = (this.rotated + 1) % 4;
			this.rotateDelay = 5;
		}
	}

	public static Shape getShape(int x, int y)
	{
		for (int i = 0; i < Handler.object.size(); i++)
		{
			if (Handler.object.get(i).getClass() == Shape.class)
			{
				Shape shape = (Shape) Handler.object.get(i);
				for (int j = 0; j < shape.squares.size(); j++)
				{
					Square s = shape.squares.get(j);
					if (s == null)
					{
						LOGGER.warning("The square was null!"); //$NON-NLS-1$
						continue;
					}
					if (x > s.x && x < s.x + size && y > s.y && y < s.y + size)
					{
						return shape;
					}
				}
			}
		}
		return null;
	}

	public void setMovingDelay(int movingDelay)
	{
		this.movingDelay = movingDelay;
	}

	public int getMovingDelay()
	{
		return this.movingDelay;
	}

	public boolean isDown()
	{
		return this.down;
	}

	public void setDown(boolean down)
	{
		this.down = down;
	}

	public List<Square> getSquares()
	{
		return this.squares;
	}

	public void setSquares(List<Square> squares)
	{
		this.squares = squares;
	}

}
